package com.example.homework.model;


import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
public class Article {
    private static int count = 0;
    private int id;
    private String title;
    private String description;
    private String picture;

    public Article(String title, String description) {
        this.id=count++;
        this.title = title;
        this.description = description;
    }

    public Article(String title, String description, String picture) {
        this.title = title;
        this.description = description;
        this.picture = picture;
    }

    public Article(int id, String title, String description, String picture) {
        this.id=id;
        this.title = title;
        this.description = description;
        this.picture = picture;
    }
    public String getPicturePath() {
        if (picture == null || id < 0) return null;

        return "/photo/" + picture;
    }

}
