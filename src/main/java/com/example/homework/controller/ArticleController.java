package com.example.homework.controller;

import com.example.homework.model.Article;
import com.example.homework.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ArticleController {
    private final ArticleService articleService;
    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;

    }


    @GetMapping("/")
    public String getArticle(Model model){
        model.addAttribute("articles",articleService.getArticleList());
        return "MainPage";
    }


    @GetMapping("/getAddArticlePage")
    public String getAddArticlePage(Model model){
        Article article = new Article();
        model.addAttribute("articleFromForm",article);
        return "articleForm";
    }


    @PostMapping("/addArticle")
    public String addArticle(@ModelAttribute(value = "articleFromForm") Article article,@RequestParam("image") MultipartFile multipartFile){
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        Path path = Paths.get("src/main/resources/files/"+fileName);
        while (true){
            try {
                Files.copy(multipartFile.getInputStream(),path, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException n){
                if (new File("src/main/resources/files/").mkdirs()){
                    continue;
                }
            }
            break;
        }
        article.setPicture(fileName);
        articleService.saveArticle(article);
        return "redirect:/";
    }

    @PostMapping("/updateArticle")
    public String updateArticle(@ModelAttribute(value = "updateArticleForm") Article updatedArticle,@RequestParam("updateImage") MultipartFile multipartFile){
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        Path path = Paths.get("src/main/resources/files/"+fileName);
        while (true){
            try {
                Files.copy(multipartFile.getInputStream(),path, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException n){
                if (new File("src/main/resources/files/").mkdirs()){
                    continue;
                }
            }
            break;
        }

        updatedArticle.setPicture(fileName);
        articleService.updateArticle(updatedArticle);
        return "redirect:/";
    }


    @GetMapping("/deleteArticle/{id}")
    public String deleteArticle(@PathVariable("id") String id){
        articleService.deleteArticleById(id);
        return "redirect:/";
    }


    @GetMapping("/getViewArticlePage")
    public String getViewArticlePage(@RequestParam(value = "title") String title,@RequestParam(value = "description") String description,Model model){
        model.addAttribute("title",title);
        model.addAttribute("description",description);
        return "viewArticle";
    }


    @GetMapping("/testArticle")
    public String initArticle(){
        List<Article> articleList = new ArrayList<>();
        articleList.add(new Article("test title0","test dec0"));
        articleList.add(new Article("test title1","test dec1"));
        articleList.add(new Article("test title2","test dec2"));
        articleService.initArticle(articleList);
        return "redirect:/";
    }

    @GetMapping("/getEditArticlePage")
    public String getEditArticlePage(@RequestParam(value = "editTitle") String title,@RequestParam(value = "editDescription") String description,@RequestParam(value = "editPicture") String picture,Model model){
        Article article = new Article(title,description,picture);
        model.addAttribute("updateArticleForm",article);
        return "editArticle";
    }
}
